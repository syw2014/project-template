# Project Template

Table of Contents
=================
* [Project Structure Description](project-structure-description)

## Project Structure Description
-- doc/
    -- 2017-nips/
        -- preamble/
        -- img/
        -- main.pdf
        -- main.tex
        -- introduction.tex
-- etc/
    -- 2017-03-25-whiteboard.jpg
    -- 2017-04-03-whiteboard.jph
    -- 2017-04-06-dustion-comments.md
    -- 2017-04-08-dave-comments.pdf
--src/
    -- checkpoints/
    -- codebase/
    -- log/
    -- out/
    -- script1.py
--README.md
